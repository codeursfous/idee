########################################
#Journal r�alis� par kevin et alexandre#
########################################

#####################################################################################################
#Objectif : But de voir l'avancement du projet et regarder ce qu'il reste � faire et de ce qui a �t�# 
#           d�j� fait                                                                               #
#####################################################################################################

#####################################################################
03/09/2015 - Cr�ation du site
	   - Mis en place du git
	   - Recherche d'une id�e du site
	   - Mis en place du drive, cr�ation de mail pour les admins
#####################################################################
04/09/2015 - Mis en place de la BDD
	   - Cr�ation de l'arborescence du site
	   - Mis en place du fichier de connection, index et accueil
#####################################################################
07/09/2015 - Mis en place du formulaire d'inscription
#####################################################################
08/09/2015 - Mis en place du journal du site
#####################################################################
10/09/2015 - Cr�ation de fonctions dans la class User
	     - insertUser
	     - deleteUser
	     - verifUser
             - userConn
#####################################################################
11/09/2015 - Continue les fonctions de la class User
	   - Cr�ation des fichiers script.js et style.css
	   - Cr�ation de la fonction de la v�rification des champs (script.js)
#####################################################################
12/09/2015 - Verification des champs en js
	   - Affichage des erreurs ou affichage de message des champs bien remplis
	   - Mis en place du CSS
#####################################################################
13/09/2015 - Continue a mettre en place le CSS
