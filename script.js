﻿function verifFormInscript(){
	var error = 0;
	
	var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
	
	var date = document.getElementsByName("date")[0];
	var dateValue = date.options[date.selectedIndex].value;
	
	var prenom = document.getElementsByName("prenom")[0].value;
	var nom = document.getElementsByName("nom")[0].value;
	
	var mois = document.getElementsByName("mois")[0];
	var moisValue = mois.options[mois.selectedIndex].value;
	
	var annee = document.getElementsByName("annees")[0];
	var anneeValue = annee.options[annee.selectedIndex].value;
	
	var mail = document.getElementsByName("mail")[0].value;
	var cMail = document.getElementsByName("confirmMail")[0].value;
	
	var pseudo = document.getElementsByName("pseudo")[0].value;
	
	var mdp = document.getElementsByName("mdp")[0].value;
	var cMdp = document.getElementsByName("confirmMDP")[0].value;
	
	if(prenom == ""){
		document.getElementById("prenom").innerHTML = "Veuillez remplir le champ prénom";
		document.getElementById("prenom").style.color = "red";
		document.getElementById("prenom").style.display = "inline";
		document.getElementById("prenom").style.fontSize = "13px";
		error++;
	}
	else{
		document.getElementById("prenom").innerHTML = "OK";
		document.getElementById("prenom").style.color = "green";
		document.getElementById("prenom").style.display = "inline";
		document.getElementById("prenom").style.fontSize = "13px";
		error--;
	}
	
	if(nom == ""){
		document.getElementById("pnom").innerHTML = "Veuillez remplir le champ nom";
		document.getElementById("nom").style.color = "red";
		document.getElementById("nom").style.display = "inline";
		document.getElementById("nom").style.fontSize = "13px";
		error++;
	}
	else{
		document.getElementById("nom").innerHTML = "OK";
		document.getElementById("nom").style.color = "green";
		document.getElementById("nom").style.display = "inline";
		document.getElementById("nom").style.fontSize = "13px";
		error--;
	}
	
	if(dateValue == "JOUR" || moisValue == "MOIS" || anneeValue == "ANNEES"){
		document.getElementById("ddn").innerHTML = "Veuillez remplir convenablement votre date de naissance";
		document.getElementById("ddn").style.color = "red";
		document.getElementById("ddn").style.display = "inline";
		document.getElementById("ddn").style.fontSize = "13px";
		error++;
	}
	else{
		document.getElementById("ddn").innerHTML = "OK";
		document.getElementById("ddn").style.color = "green";
		document.getElementById("ddn").style.display = "inline";
		document.getElementById("ddn").style.fontSize = "13px";
		error--;
	}
	
	if(reg.test(mail)){
		document.getElementById("mail").innerHTML = "OK";
		document.getElementById("mail").style.color = "green";
		document.getElementById("mail").style.display = "inline";
		document.getElementById("mail").style.fontSize = "13px";
		error--;
	}
	else{
		document.getElementById("mail").innerHTML = "Veuillez saisir une adresse mail valide";
		document.getElementById("mail").style.color = "red";
		document.getElementById("mail").style.display = "inline";
		document.getElementById("mail").style.fontSize = "13px";
		error++;
	}
	
	if(mail == cMail && reg.test(cMail)){
		document.getElementById("cMail").innerHTML = "OK";
		document.getElementById("cMail").style.color = "green";
		document.getElementById("cMail").style.display = "inline";
		document.getElementById("cMail").style.fontSize = "13px";
		error--;
	}
	else{
		document.getElementById("cMail").innerHTML = "Les adresses mails sont différentes";
		document.getElementById("cMail").style.color = "red";
		document.getElementById("cMail").style.display = "inline";
		document.getElementById("cMail").style.fontSize = "13px";
		error++;
	}
	
	if(pseudo.length < 5){
		document.getElementById("pseudo").innerHTML = "Le pseudo doit contenir au mnimum 5 caractères";
		document.getElementById("pseudo").style.color = "red";
		document.getElementById("pseudo").style.display = "inline";
		document.getElementById("pseudo").style.fontSize = "13px";
		error++;
	}
	else{
		document.getElementById("pseudo").innerHTML = "OK";
		document.getElementById("pseudo").style.color = "green";
		document.getElementById("pseudo").style.display = "inline";
		document.getElementById("pseudo").style.fontSize = "13px";
		error--;
	}
	
	if(mdp.length < 5){
		document.getElementById("mdp").innerHTML = "Le mot de pass doit contenir au mnimum 5 caractères";
		document.getElementById("mdp").style.color = "red";
		document.getElementById("mdp").style.display = "inline";
		document.getElementById("mdp").style.fontSize = "13px";
		error++;
	}
	else{
		document.getElementById("mdp").innerHTML = "OK";
		document.getElementById("mdp").style.color = "green";
		document.getElementById("mdp").style.display = "inline";
		document.getElementById("mdp").style.fontSize = "13px";
		error--;
	}
	
	if((mdp == cMdp) && cMdp.length >= 5){
		document.getElementById("cMDP").innerHTML = "OK";
		document.getElementById("cMDP").style.color = "green";
		document.getElementById("cMDP").style.display = "inline";
		document.getElementById("cMDP").style.fontSize = "13px";
		error--;
	}
	else{
		document.getElementById("cMDP").innerHTML = "Les 2 mots de pass sont différents";
		document.getElementById("cMDP").style.color = "red";
		document.getElementById("cMDP").style.display = "inline";
		document.getElementById("cMDP").style.fontSize = "13px";
		error++;
	}
	
	if(error == 0)
		return true;
	else
		return false;
}

function verifFormConnect(){


}












